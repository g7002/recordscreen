import React, { Component } from "react";
import "./App.css";
import Home from "./components/Home";
import Recorder from "./components/Recorder";
import { FaMoon, FaSun } from "react-icons/fa";
import {
  signInWithGooglePopup,
  createUserDocumentFromAuth,
  auth,
} from "./utils/firebase/firebaseUtils";
import { Button } from "@chakra-ui/react";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recorder: false,
      isSignedIn: false,
      theme: false,
      userID: null,
      user: {},
    };
  }
  componentDidMount = () => {
    this.userSessio();
  };
  changeRecorder = (recorder) => {
    this.setState({
      recorder,
    });
  };
  approveUser = (userID) => {
    this.setState({
      isSignedIn: true,
      userID,
    });
  };
  changeTheme = () => {
    this.setState({ theme: !this.state.theme });
  };
  logGoogleUser = async () => {
    const { user } = await signInWithGooglePopup();
    const userDocRef = await createUserDocumentFromAuth(user);
    if (userDocRef) {
      this.setState({ user: "user" }, () => {
        this.approveUser(userDocRef.id);
      });
    } else return;
  };
  handleSavedRecording = (savedVideo) => {
    this.setState({ savedVideo });
  };
  userSessio = () => {
    auth.onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user: user });
      } else {
        this.setState({ user: null });
      }
    });
  };
  logOut = async () => {
    await auth.signOut();
    this.userSessio();
  };
  render() {
    const { recorder, isSignedIn } = this.state;
    return (
      <div
        style={{
          background: this.state.theme ? "#1A202C" : "white",
          color: !this.state.theme ? "#1A202C" : "white",
          minHeight: "100vh",
          paddingBottom: "10px",
        }}
      >
        <div
          style={{
            display: "flex",
            paddingTop: "30px",
            paddingRight: "30px",
            fontSize: "30px",
            marginLeft: "auto",
            justifyContent: "flex-end",
            width: "100%",
          }}
        >
          {this.state.user ? (
            <Button
              variant={"outline"}
              colorScheme={"purple.100"}
              height={"auto"}
              size={"sm"}
              p="3"
              mr={3}
              borderRadius={"50px"}
              onClick={this.logOut}
              w={20}
            >
              SIGN OUT!
            </Button>
          ) : (
            <Button
              variant={"outline"}
              colorScheme={"purple.100"}
              height={"auto"}
              size={"sm"}
              p="3"
              mr={3}
              borderRadius={"50px"}
              onClick={this.logGoogleUser}
              w={20}
            >
              SIGN IN!
            </Button>
          )}
          {this.state.theme ? (
            <FaSun
              onClick={this.changeTheme}
              className={"icon"}
              style={{
                height: "100%",
                color: "#9b4dca",
                border: "2px solid #9b4dca",
                padding: "3px",
                alignSelf: "center",
                borderRadius: "50%",
              }}
            />
          ) : (
            <FaMoon
              onClick={this.changeTheme}
              style={{
                color: "#9b4dca",
                border: "2px solid #9b4dca",
                padding: "3px",
                alignSelf: "center",
                borderRadius: "50%",
              }}
            />
          )}
        </div>
        {!recorder ? (
          <Home
            changeRecorder={this.changeRecorder}
            // isSignedIn={isSignedIn}
            approveUser={this.approveUser}
            userID={this.state.userID}
            userSession={this.state.user}
          />
        ) : (
          <Recorder
            userID={this.state.userID}
            changeRecorder={this.changeRecorder}
            userSession={this.state.user}
          />
        )}
      </div>
    );
  }
}

export default App;
