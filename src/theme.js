import { extendTheme } from "@chakra-ui/react";

const config = {
  initialColorMode: "Light",
  useSystemColorMode: true,
};

export const customTheme = extendTheme({
  colors: {
    purple: {
      100: "#9b4dca",
    },
    gray: {
      100: "#606c76",
    },
  },
  components: {
    Button: {
      baseStyle: {
        fontWeight: "bold",
        borderRadius: "3px",
      },
      sizes: {
        sm: {
          fontSize: "12px",
        },
        md: {
          fontSize: "11px",
          fontWeight: "700",
          letterSpacing: "1px",
        },
      },
      variants: {
        outline: {
          color: "purple.100",
          px: "31px",
          py: "12px",
          _active: {
            transform: "none",
            background: "none",
          },
          _focus: {
            transition: "none",
            boxShadow: "none",
          },
          _hover: { background: "none", color: "gray.100" },
        },
        ghost: {
          color: "purple.100",
          height: "auto",
          py: "10px",
          background: "none",
          transition: "0",
          _active: {
            transform: "none",
            background: "none",
          },
          _focus: {
            transition: "none",
            boxShadow: "none",
          },
          _hover: { background: "none", color: "gray.100" },
        },
        solid: {
          height: "auto",
          color: "white",
          backgroundColor: "purple.100",
          px: "31px",
          py: "12px",
          border: "none",
          transition: "0",

          _active: {
            background: "purple.100",
            transition: "none",
          },
          _focus: {
            boxShadow: "none",
          },
          _hover: { bg: "gray" },
        },
      },
      defaultProps: {
        size: "md",
        variant: "solid",
      },
    },
  },

  textStyles: {
    h1: {
      fontSize: "46px",
      fontWeight: "300",
      lineHeight: "1.3",
      letterSpacing: "-.8px",
      wordWrap: "normal",
    },
    h2: {
      fontSize: "22px",
      fontWeight: "300",
      lineHeight: "1.3",
      letterSpacing: "-.8px",
    },
    h6: {
      fontSize: "13px",
      fontWeight: "300",
      lineHeight: "1.8",
      letterSpacing: "-.2px",
    },
  },
});

export const theme = extendTheme({ config });
