import { initializeApp } from "firebase/app";
import { getAuth, signInWithPopup, GoogleAuthProvider,setPersistence,browserLocalPersistence, } from "firebase/auth";

import { getFirestore, doc, getDoc, setDoc, addDoc, collection, getDocs } from "firebase/firestore";

import { getStorage, ref, uploadString, uploadBytes, getDownloadURL, deleteObject  } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyBnSh16u8WsRbpjf2OKUsXY4fFGAFtrI9k",
  
  authDomain: "record-screen-project.firebaseapp.com",

  databaseURL: "https://record-screen-project-default-rtdb.firebaseio.com/",

  projectId: "record-screen-project",

  storageBucket: "record-screen-project.appspot.com",

  messagingSenderId: "643870007189",

  appId: "1:643870007189:web:b4bb09c62ea79f1026609c"
};

export const firebaseApp = initializeApp(firebaseConfig);

const googleProvider = new GoogleAuthProvider();

googleProvider.setCustomParameters({
  prompt: "select_account",
});
//  firebaseApp;
export const auth = getAuth(firebaseApp);
export const signInWithGooglePopup = async () =>{
  await setPersistence(auth,browserLocalPersistence);
  return signInWithPopup(auth, googleProvider);
};

export const db = getFirestore();

export const createUserDocumentFromAuth = async (userAuth) => {
  if (!userAuth) return;
  const userDocRef = doc(db, "users", userAuth.uid);

  const userSnapshot = await getDoc(userDocRef);
  if (!userSnapshot.exists()) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await setDoc(userDocRef, {
        displayName,
        email,
        createdAt,
      });
    } catch (error) {
      console.log("error occured : ", error);
    }
  }
  return userDocRef;
};


const storage = getStorage(firebaseApp);






console.log('Running firebase Utils');


let showRecordingURLs = async () => {
  const querySnapshot = await getDocs(collection(db, "recordingURLs"));
  querySnapshot.forEach((doc) => {
    console.log(`${doc.data().id} has video ${doc.data().fullPath}`);
  });
};

let deleteRecordingURL = (fullPath = 'videos/video4.webm') => {
  deleteObject(ref(storage, fullPath)).then(() => {
    console.log('deleted')
  }).catch((error) => {
    console.log('errror deleting', error);
  });
};

let getRecordingDownloadURL = async (fullPath = 'videos/video4.webm') =>{
  getDownloadURL(ref(storage, fullPath))    // errror ?????
    .then((url) => {
      console.log(`URL of ${fullPath} is ${url}`);
    })
    .catch((error) => {
      console.log('error in getting downloadingURL', error);
    }); 
};

export const getUserRecordings = async (id = 'test id 2') => {
  const querySnapshot = await getDocs(collection(db, "recordingURLs"));
  let returnArr = []; //console.log('kkkk');
  let pathArr = [];
  querySnapshot.forEach(doc => {
    if(doc.data().id == id){
      pathArr.push(doc.data().fullPath);
    }
  });

  for(let i = 0; i < pathArr.length; i++){
   

      let url = await getDownloadURL(ref(storage, pathArr[i]));    // errror ?????
      returnArr.push({
        filename: pathArr[i].split('/')[1].split('.')[0],
        url: url,
      });
  }
 
  return returnArr;
};

let updateDatabase = async (id, fullPath) => {
  const docRef = await addDoc(collection(db, "recordingURLs"), {
    id,
    fullPath,
  });    
  console.log(`${fullPath} info added in database`);
};

export function uploadBlob(blob, id, fileName = `video-${new Date().getDate()}-${new Date().getTime()} .webm`){
  const storageRef = ref(storage, `videos/${fileName}`);
  uploadBytes(storageRef, blob).then((snapshot) => {      
    console.log(`${fileName} Uploaded at ${snapshot.ref.fullPath}`);
    updateDatabase(id, snapshot.ref.fullPath);
  });
}