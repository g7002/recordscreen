import React, { Component } from "react";
import { Box, Text, Button, VStack } from "@chakra-ui/react";

class StreamRecording extends Component {
  constructor(props) {
    super(props);
    this.videoRef = React.createRef();
  }
  componentDidMount() {
    this.props.start();
  }
  componentDidUpdate() {
    this.updateVideoStream();
  }

  updateVideoStream() {
    if (this.videoRef.current.srcObject !== this.props.stream) {
      this.videoRef.current.srcObject = this.props.stream;
    }
  }
  render() {
    if (!this.props.isPlaying) {
      return (
        <Box
          mt={"18vh"}
          ml={{ base: "28%", sm: "28%", md: "31%" }}
          border={"1px solid rgba(201,186,210,.25)"}
          p={"20px"}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          rounded="5px"
          w={{ base: "44%", sm: "44%", md: "37%" }}
          color="#606c76"
        >
          <Box textStyle={"h2"} textAlign="center" mb={5}>
            Getting permissions for mic and screen...
          </Box>
        </Box>
      );
    }

    return (
      <Box
        color="#606c76"
        mt="18vh"
        w={{ base: "47%", sm: "47%", md: "48%", xl: "38%" }}
        mx="auto"
        border={"1px solid rgba(201,186,210,.25)"}
        boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
        rounded="5px"
        p={5}
        textAlign="center"
      >
        <Box
        textStyle={"h2"}
         mb={4}>
          Recording in progress
        </Box>

        <Box
          as="video"
          ref={this.videoRef}
          autoPlay
          muted
          type="video/webm"
          objectFit="contain"
        />
        <VStack
        mt={2}
        spacing={5}
        alignItems="flex-start"
        >
          {this.props.isCameraError && (
            <Text textAlign={"left"} >
              Could not get camera stream. Did you give camera permissions?
            </Text>
          )}
          {this.props.isAudioError && (
            <Text textAlign={"left"}>
              Could not get microphone stream. Did you give microphone
              permissions?
            </Text>
          )}
        </VStack>
        <Button
          mt={10}
          px={12}
          alignSelf={"flex-end"}
          onClick={this.props.stop}
        >
          {" "}
          Stop Recording
        </Button>
      </Box>
    );
  }
}

export default StreamRecording;
