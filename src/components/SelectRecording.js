import React, { Component } from "react";
import { Button, Text, Flex, VStack, Box } from "@chakra-ui/react";

class SelectRecording extends Component {
  constructor(props) {
    super(props);
    this.state = {
      canMove: false,
      offsetX: 0,
      offsetY: 0,
      translateX: 0,
      translateY: 0,
    };
  }
  moveCamera = (event) => {
    const viewportWidth = window.innerWidth;
    const viewportHeight = window.innerHeight;
    const cameraHeight = 230;
    const cameraWidth = 230;
    let x = event.clientX - this.state.offsetX;
    let y = event.clientY - this.state.offsetY;

    if (y < 0) {
      y = 0;
    } else if (y + cameraHeight > viewportHeight) {
      y = viewportHeight - cameraHeight;
    }

    if (x < 0) {
      x = 0;
    } else if (x + cameraWidth > viewportWidth) {
      x = viewportWidth - cameraWidth;
    }
    this.setState({
      translateX: x,
      translateY: y,
    });
  };
  mouseDownEvent = (event) => {
    this.setState({
      canMove: true,
      offsetX: event.nativeEvent.offsetX,
      offsetY: event.nativeEvent.offsetY,
    });
  };
  render() {
    const {
      screenAndCamera,
      changeRecordingType,
      changeRecorder,
      changeStatus,
    } = this.props;
    return (
      <Box
        onMouseUp={() => this.setState({ canMove: false })}
        onMouseMove={this.state.canMove ? this.moveCamera : null}
        h="100vh"
        pt={"26vh"}
      >
        <VStack
          textAlign={"center"}
          spacing={4}
          p={5}
          rounded={5}
          ml={{ base: "28%", sm: "28%", md: "25%", lg: "26%", xl: "31%" }}
          w={{ base: "44%", sm: "45%", md: "50%", lg: "45%", xl: "38%" }}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          border={"1px solid rgba(201,186,210,.25)"}
          color="gray.100"
        >
          <Box textStyle={"h2"} alignSelf={"flex-start"} textAlign={"left"}>
            {screenAndCamera ? "Record Screen + Camera" : "Record Screen"}
          </Box>
          <Flex
            flexWrap={"wrap"}
            w={{ base: "60%", sm: "60%", md: "80%", lg: "70%", xl: "67%" }}
            _hover={{
              color: "#9b4dca",
            }}
          >
            <VStack
              m={"auto"}
              mt={4}
              textAlign={"center"}
              className={
                !screenAndCamera ? "icon-container" : "icon-container dim-icon"
              }
              onClick={() => changeRecordingType(true)}
              _hover={{
                cursor: "pointer",
              }}
            >
              <img
                src="/images/cameraAndScreenIcon.png"
                style={{ width: "60px" }}
              />
              <Text fontSize={16} mt={0}>
                {" "}
                Screen + Cam{" "}
              </Text>
            </VStack>

            <VStack
              m={"auto"}
              mt={4}
              textAlign={"center"}
              _hover={{
                cursor: "pointer",
              }}
              className={
                screenAndCamera ? "icon-container" : "icon-container dim-icon"
              }
              onClick={() => changeRecordingType(false)}
            >
              <img src="/images/screenOnlyIcon.png" style={{ width: "60px" }} />
              <Text style={{ marginTop: 0 }} fontSize={16}>
                {" "}
                Screen Only{" "}
              </Text>
            </VStack>
          </Flex>
          <Flex
            wrap={"wrap"}
            justifyContent={"center"}
            textAlign="center"
            style={{ marginTop: "40px" }}
          >
            <Button
              variant={"ghost"}
              size={"sm"}
              onClick={() => changeRecorder(false)}
              mx={5}
            >
              CANCEL
            </Button>
            <Button
              bg={"#9b4dca"}
              color="white"
              onClick={() =>
                changeStatus(this.state.translateX, this.state.translateY)
              }
            >
              START RECORDING
            </Button>
          </Flex>
          <Flex
            className="camera"
            style={
              screenAndCamera
                ? {
                    display: "flex",
                    transform: `translate(${this.state.translateX}px ,${this.state.translateY}px)`,
                    marginTop: "0",
                  }
                : { display: "none" }
            }
            onMouseDown={this.mouseDownEvent}
          >
            Camera <br /> Placement
          </Flex>
        </VStack>
      </Box>
    );
  }
}

export default SelectRecording;
