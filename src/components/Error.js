import React, { Component } from "react";
import { Button, Text, VStack } from "@chakra-ui/react";

export default class Error extends Component {
  render() {
    return (
      <VStack
        spacing={12}
        mt={"18vh"}
        ml={{ base: "26%", sm: "28%", md: "26%", lg: "28%", xl: "30%" }}
        p={5}
        border={"1px solid rgba(201,186,210,.25)"}
        boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
        rounded="5px"
        w={{ base: "44%", sm: "45%", md: "48%", lg: "43%", xl: "38%" }}
        textAlign="center"
        fontSize={"22px"}
        lineHeight={1.4}
        mb={5}
        letterSpacing={"-.7px"}
      >
        <Text fontSize={"22px"} color="#606c76">
          Could not get display stream. Did you cancel the screen selection
          popup?
        </Text>

        <Button
          as={"a"}
          href="/"
          fontSize="xs"
          alignSelf={"flex-end"}
          onClick={this.props.onPermissionDenied}
        >
          RELOAD
        </Button>
      </VStack>
    );
  }
}
