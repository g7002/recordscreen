import React, { Component } from "react";
import { Box, VStack, Button, Heading } from "@chakra-ui/react";
import SavedRecordings from "./SavedRecordings";

export default class PlaySavedVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savedVideo: false,
    };
  }
  render() {
    const { filename, url } = this.props.recordingInfo;
    // console.log(fileName)
    if (this.state.savedVideo) {
      return <SavedRecordings />;
    }

    return (
      <div>
        <VStack
          color="#606c76"
          mt="18vh"
          w={{ base: "50%", md: "45%", xl: "40%" }}
          mx="auto"
          border={"1px solid rgba(201,186,210,.25)"}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          rounded="5px"
          p={5}
          justifyContent={"center"}
          alignItems={"center"}
          textAlign="center"
        >
          <Heading>{filename}</Heading>
          <Box
            as="video"
            src={url}
            controls
            // type="video/webm"
            objectFit="contain"
            w={"100%"}


            

          />
          <Button mt={5} onClick={() => this.props.handlePlay(false)}>
            BACK
          </Button>
        </VStack>
      </div>
    );
  }
}
