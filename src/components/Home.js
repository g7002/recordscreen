import React, { Component } from "react";

import { Box, Button, Image, Text, Link } from "@chakra-ui/react";
import SavedRecordings from "./SavedRecordings";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savedVideo: false,
    };
  }
 
  handleSavedRecording = (savedVideo) => {
    this.setState({ savedVideo });
  };
  render() {
    if (this.state.savedVideo) {
      return (
        <SavedRecordings
          handleSavedRecording={this.handleSavedRecording}
          userID={this.props.userID}
        />
      );
    }
    return (
      <Box px={"10vw"} textAlign="center" color="#606c76">
        <Box m="auto" mt="18vh" w={"70%"} letterSpacing={"-1px"}>
          <Image src="/images/recorderIcon.png" width={"70px"} m="auto" />
          <Box textStyle="h1" mt={"6px"}>
            RecordScreen
            <Text as={"span"} fontSize={38}>
              .io
            </Text>
          </Box>
          <Box textStyle="h2" mt={5}>
            Record your screen right from the browser.
            <br />
            No installation required.
          </Box>
          <Button mt={5} onClick={this.props.changeRecorder}>
            RECORD!
          </Button>
          <br />
          {this.props.userSession && (
            <Button
              variant={"ghost"}
              mt={5}
              onClick={() => this.handleSavedRecording(true)}
              color="purple.500"
              fontSize={"xl"}
              _hover={{
                color: "purple.300",
              }}
            >
              Previous Recordings!
            </Button>
          )}
        </Box>
        <Box m="auto" mt={10} w="70%">
          <Box textStyle={"h6"} fontStyle={"italic"}>
            Videos are processed in the browser and are never sent to our
            servers.
          </Box>
          <Box mt={5} fontSize={13}>
            By{" "}
            <Link
              color={"#9b4dca"}
              href="https://codeinterview.io/"
              _hover={{ textDecoration: "none", color: "#606c76" }}
              _focus={{ boxShadow: "none" }}
            >
              CodeInterview.io
            </Link>{" "}
            team. Copyright © 2022.
          </Box>
        </Box>
      </Box>
    );
  }
}

export default Home;
