import React, { Component } from "react";
import {Box,Text,VStack,Flex,Modal,ModalOverlay,ModalContent,ModalHeader,ModalFooter,ModalBody,Button,} from "@chakra-ui/react";
import { uploadBlob } from "../utils/firebase/firebaseUtils";

class ShowRecording extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      isClose: false,
      filename: "",
    };
  }
  onOpen = () => {
    this.setState({ isOpen: true });
  };
  onClose = () => {
    this.setState({ isClose: true, isOpen: false });
  };
  handleChange = (e) => {
    this.setState({ filename: e.target.value });
  };
  handleSubmit = (e) => {
    e.preventDefault();
      uploadBlob(this.props.blob, this.props.userID,this.state.filename);
    this.setState({ fileName: "" }, () => {
      this.onClose();
    });
  };
  render() {
    let filename = "recorded-video";
    return (
      <>
        <VStack
          color="gray.100"
          mt="18vh"
          w={{ base: "45%", sm: "47%", md: "47%", lg: "44%", xl: "38%" }}
          mx="auto"
          border={"1px solid rgba(201,186,210,.25)"}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          rounded={5}
          p={5}
          justifyContent={"center"}
          alignItems={"center"}
          textAlign="center"
        >
          <Text fontSize="22px" mb={3} letterSpacing={"-1px"}>
            Download Video
          </Text>

          <Box
            as="video"
            src={this.props.downloadVideoURL}
            controls
            type="video/webm"
            objectFit="contain"
            style={{ marginBottom: "25px" }}
          />
          <Flex
            flexWrap={"wrap"}
            justify={"center"}
            w={{ base: "100%", sm: "100%", md: "100%", lg: "100" }}
            textAlign="center"
          >
            <Button
              as={"a"}
              href={this.props.downloadVideoURL}
              download={`${filename}.webm`}
              style={{ textDecoration: "none" }}
              mr={"10px"}
            >
              DOWNLOAD VIDEO{" "}
            </Button>
            <Button
              variant={"outline"}
              colorScheme={"purple.100"}
              height={"auto"}
              onClick={() => this.props.changeRecorder(false)}
            >
              {" "}
              CLOSE
            </Button>
          </Flex>
          <Button
            onClick={this.onOpen}
            backgroundColor={"#3b1055"}
            disabled={!this.props.userSession ? "disabled" : ""}
          >
            {" "}
            UPLOAD{" "}
          </Button>
        </VStack>

        <Modal isOpen={this.state.isOpen} onClose={this.state.isClose}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader>Enter File Name To Upload</ModalHeader>
            <ModalBody>
              <form onSubmit={this.handleSubmit}>
                <input
                  style={{ color: "black" }}
                  type={"text"}
                  placeholder="enter file name"
                  value={this.state.filename}
                  onChange={this.handleChange}
                />
                <Button
                  ml={2}
                  py={2}
                  varient="ghost"
                  type="submit"
                  onClick={this.handleSubmit}
                >
                  CONFIRM!
                </Button>
              </form>
            </ModalBody>

            <ModalFooter>
              <Button colorScheme="blue" mr={3} onClick={this.onClose}>
                CLOSE
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </>
    );
  }
}

export default ShowRecording;
