import React, { Component } from "react";
import RecordRTC from "recordrtc";
import SelectRecording from "./SelectRecording";
import StreamRecording from "./StreamRecording";
import ShowRecording from "./ShowRecording";
import Error from "./Error";

class Recorder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isScreenAndCamera: true,
      isSelected: false,
      isPlaying: false,
      isSaved: false,
      stream: null,
      streamArray: null,
      blob: null,
      recordingURL: null,
      positionX: 0,
      positionY: 0,
      isCameraError: false,
      isAudioError: false,
      isScreenError: false
    };
  }
  recorder;
  changeRecordingType = isScreenAndCamera => {
    this.setState({
      isScreenAndCamera,
    });
  };
  startRecording = (positionX, positionY) => {
    this.setState({
      isSelected: true,
      positionX,
      positionY
    });
  };
  start = async () => {  
    const streamArray = [];
    let cameraStream;
    let videostream;
    let audioStream;
    let isCameraError = false;
    let isAudioError = false;

    if (this.state.isScreenAndCamera) {
      try{
        cameraStream = await navigator.mediaDevices.getUserMedia({
          video: true
        });
        streamArray.push(cameraStream);
      }
      catch(error){
        isCameraError = true;
      }
    }

    try{
      audioStream = await navigator.mediaDevices.getUserMedia({
        audio: true
      });
      streamArray.push(audioStream);
    }
    catch(error){
      isAudioError = true;
    }

    try{
      videostream = await navigator.mediaDevices.getDisplayMedia({
        video: true
      });
      streamArray.push(videostream);
    }
    catch(error){
      this.setState({
        isScreenError: true
      });
      return;
    }

    if (this.state.isScreenAndCamera && !isCameraError) {
      cameraStream.width = 320;
      cameraStream.height = 240;  
      // cameraStream.radius = '50%';
      cameraStream.top = this.state.positionY; //screen.height - cameraStream.height;
      cameraStream.left = this.state.positionX; //screen.width - cameraStream.width;
    }
    videostream.width = window.screen.width;
    videostream.height = window.screen.height;
    videostream.fullcanvas = true;

    this.recorder = RecordRTC(streamArray, {
      type: 'video',
      mimeType: 'video/webm',
      previewStream: stream => {
        this.setState({
          stream,
          streamArray,
          isPlaying: true,
          isCameraError,
          isAudioError
        });
      },
    });

    this.recorder.startRecording();
  };
  stop = () => {
    this.recorder.stopRecording(() => {
      const blob = this.recorder.getBlob();
      this.state.streamArray.forEach(stream => {
        stream.getTracks().forEach(track =>  track.stop());
      });
      this.setState({
        blob,
        recordingURL: URL.createObjectURL(blob),
        isPlaying: false,
        isSaved: true,
      });
    });
  };

  render() {
    if(this.state.isScreenError){
      return <Error/>;
    }
    if(this.state.isSaved) {
      return (
        <ShowRecording userID={this.props.userID} blob={this.state.blob} downloadVideoURL={this.state.recordingURL} changeRecorder={this.props.changeRecorder} userSession={this.props.userSession}/>
      );
    }
    return (
      <div>
        {
        !this.state.isSelected
          ? <SelectRecording screenAndCamera={this.state.isScreenAndCamera} changeRecordingType={this.changeRecordingType} changeRecorder={this.props.changeRecorder} changeStatus={this.startRecording} />
          : <StreamRecording isPlaying={this.state.isPlaying} stream={this.state.stream} isCameraError={this.state.isCameraError} isAudioError={this.state.isAudioError} start={this.start} stop={this.stop}/>
        }
      </div>
    );
  }
}

export default Recorder;
