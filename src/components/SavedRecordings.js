import { Box, HStack, Heading, Image, Text, Button } from "@chakra-ui/react";
import React, { Component } from "react";
import PlaySavedVideo from "./PlaySavedVideo";
import { getUserRecordings } from "../utils/firebase/firebaseUtils";

export default class SavedRecordings extends Component {
  state = {
    recordings: [],
    isRecordingFetched: false,
    recordingToPlay: false,
    recordingInfo: null,
  };
  componentDidMount() {
    getUserRecordings(this.props.userID).then((arr) =>
      this.setState({
        recordings: arr,
        isRecordingFetched: true,
      })
    );
  }

  handlePlay = (recordingToPlay) => {
    this.setState({ recordingToPlay });
  };
  render() {
    if (!this.state.isRecordingFetched) {
      return (
        <Box
          color="gray.100"
          mt="18vh"
          mx="auto"
          border={"1px solid rgba(201,186,210,.25)"}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          rounded={5}
          p={5}
          textAlign="center"
          w={{ base: "50%", md: "45%", xl: "40%" }}
        >
          <Heading>Loading....</Heading>
        </Box>
      );
    }
    if (this.state.recordingToPlay) {
      console.log(this.state.recordingToPlay);
      return (
        <PlaySavedVideo
          handlePlay={this.handlePlay}
          recordingInfo={this.state.recordingInfo}
        />
      );
    }
    return (
      <div>
        <Box
          color="gray.100"
          mt="18vh"
          mx="auto"
          border={"1px solid rgba(201,186,210,.25)"}
          boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
          rounded={5}
          p={5}
          textAlign="center"
          w={{ base: "50%", md: "45%", xl: "40%" }}
        >
          <Heading>Recording Collection</Heading>
          {this.state.recordings.map((recording) => {
            return (
              <Box
                key={recording.filename}
                mt={15}
                textDecoration={"none"}
                onClick={() =>
                  this.setState({
                    recordingToPlay: true,
                    recordingInfo: recording,
                  })
                }
              >
                <HStack
                  
                  boxShadow={" 17px 17px 47px -13px rgb(0 0 0 / 12%)"}
                  opacity=".7"
                  _hover={{
                    cursor: "pointer",
                    opacity: "1",
                  }}
                  w={"90%"}
                  m={"auto"}
                >
                  <Image src="/images/videoListIcon.png" width={"70px"} />
                  <Text textAlign={"left"} textDecoration={"none"} w={"auto"}>
                    {recording.filename}
                  </Text>
                </HStack>
              </Box>
            );
          })}
          <Button mt={5} onClick={() => this.props.handleSavedRecording(false)}>
            BACK TO HOME
          </Button>
        </Box>
      </div>
    );
  }
}
